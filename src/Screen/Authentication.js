import React, { Component, useState, useEffect } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ImageBackground, ScrollView, TextInput} from 'react-native'
import Logo from '../assets/logosplashscreen.png'
import styles from '../styling/styling'
import first from '../assets/firstpage.png'
import second from '../assets/secondpage.png'
import MainText from '../component/MainText'
import SecondaryText from '../component/SecondaryText'
import Button from '../component/button'
import AsyncStorage from '@react-native-async-storage/async-storage';
import { connect } from 'react-redux';

import fetchUser from '../stores/actions/authActions'

function Authentication(props){
    const { navigation } = props
    const [username, setUsername] = useState('')

    useEffect(() => {
        // props.authStore.payload.data.token = ''
        getData()
    }, [])

    const getData = async() => {
        try {
          const value = await AsyncStorage.getItem('@token')
          if(value !== null) {
            // AsyncStorage.setItem('@token', props.authStore.payload.data.token)
            console.log('token : ' + value)
          } 
        } catch(e) {
            console.error(e)
        }
    }

    
    useEffect(() => {
        console.log(props.authStore.payload.data)
        // const {navigation} = props
        // if(props.authStore.payload.data.token && props.authStore.payload.store !== ''){
        //     AsyncStorage.setItem('@token', props.authStore.payload.data.token)
        //     AsyncStorage.setItem('@username', props.authStore.payload.data.username)
        //     console.log(username)
        //     navigation.navigate('Home')
        // }
    }, [props.authStore.payload])

    const handleLogin = async(username) => {
        try{
            await props.dispatchLogin(username)
            if(props.authStore.payload.data.token && props.authStore.payload.store !== ''){
                AsyncStorage.setItem('@token', props.authStore.payload.data.token)
                AsyncStorage.setItem('@username', props.authStore.payload.data.username)
                console.log(username)
                navigation.navigate('Home')
            }
            // console.log('==handleLogin ', val);
        } catch (e) {
            console.log(e)
        }
    }

    return(
        <ScrollView style={{flex:1, backgroundColor : '#FFFFFF'}}>
            <View style={{backgroundColor:'#FFA451', flex:0.6, justifyContent:'center', alignItems:'center', alignContent:'center'}}>
                <Image source={second} style={{height: 301, width : 301, marginTop : 155, marginBottom: 34}}/>
            </View>
            <View style={{flex:0.4, paddingLeft : 25, paddingRight : 25}}>
                <MainText text='What is your firstname?' customStyle={{textAlign:'left' , marginTop : 56}}/>
                <View>
                    <TextInput 
                        placeholder='First Name' 
                        placeholderTextColor='#C2BDBD' 
                        style={{backgroundColor : '#F3F1F1', 
                        borderRadius : 10, 
                        height : 56, 
                        marginTop : 16, 
                        paddingLeft : 24}}
                        onChangeText={(value) => setUsername(value)}
                        />
                </View>
                <Button buttonText={`Start Ordering`} customStyle={{marginTop : 56}}
                    onPress={() => handleLogin({username})}/>
            </View>
        </ScrollView>

    )
}

function mapStateToProps(state){
    return{
        authStore : state.authStore,
        isLoading : state.authStore.isLoading,
    }
}

function mapDispatchToProps(dispatch){
    return {
        dispatchLogin : (username) => dispatch(fetchUser(username))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Authentication)