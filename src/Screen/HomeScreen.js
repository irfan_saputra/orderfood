import React, { Component, useState, useEffect, useRef } from 'react'
import {  StyleSheet, FlatList, View, Text, TouchableOpacity, Image, ImageBackground, TextInput, ScrollView} from 'react-native'
import Logo from '../assets/logosplashscreen.png'
import { Card, Divider } from 'react-native-elements';
import styles from '../styling/styling'
import first from '../assets/firstpage.png'
import MainText from '../component/MainText'
import SecondaryText from '../component/SecondaryText'
import Button from '../component/button'
import TextInputwithIcon from '../component/TextInputWithIcon'
import Icons from 'react-native-vector-icons/Ionicons'
import AsyncStorage from '@react-native-async-storage/async-storage';
import honeylimepeach from '../assets/honeylimepeach.png'
import fetchGetProduct, { addtoFavorite } from '../stores/actions/getProductActions'
import { addToCart, subtractQuantity } from '../stores/actions/basketActions'
import { connect } from 'react-redux';
import ModalDelete from '../component/modalDelete'

function HomeScreen(props){
    const { navigation } = props
    const flatListRef = useRef()
    const [username, setUsername] = useState('')
    const [token, setToken] = useState('')
    const [list, setList] = useState([])
    const [listRecommended, setListRecommended] = useState([])
    const [listPerCategory, setListPerCategory] = useState([])
    const [listCategory, setListCategory] = useState([])
    const [modalLogoutVisible, setModalLogoutVisible] = useState(false)
    const [category, setCategory] = useState([
        {
            id : 1,
            name : 'Hottest'
        },
        {
            id : 2,
            name : 'Popular'
        },
        {
            id : 3,
            name : 'New Combo'
        },
        {
            id : 4,
            name : 'Top'
        },
    ])
    const [listBelow, setListBelow] = useState([])
    const [selectedId, setSelectedId] = useState(1);
    const [count, setCount] = useState(0)



    const [refreshing, setRefreshing] = useState(true)
    const profpic = { uri : 'https://conferenceoeh.com/wp-content/uploads/profile-pic-dummy.png' }

    useEffect(() => {
        getData()
    }, [])

    const toTop = () => {
        // use current
        flatListRef.current.scrollToOffset({ animated: true, offset: 0 })
    }

    useEffect(() => {
        if(props.getProductStore.payload.data && props.getProductStore.payload.data.foods){
            props.basketStore.items = props.getProductStore.payload.data.foods
            console.log('cobaiiinn : ', props.getProductStore.payload.data.foods)
            setList(props.getProductStore.payload.data.foods.map(function(o) {
                    o.quantity = 0
                    return o
            }))
            setListRecommended(props.getProductStore.payload.data.foods.filter(obj => {return obj.recommended == true}))
            setListPerCategory(props.getProductStore.payload.data.foods.filter(obj => {return obj.hottest == true}))
            // // let result = props.getProductStore.payload.data.foods.map(function(o) {
            // //     o.count = 0
            // //     return o
            // // })
            // // setList(props.getProductStore.payload.data.foods)
            // setListRecommended(list.filter(obj => {return obj.recommended == true}))
            // setListPerCategory(list.filter(obj => {return obj.hottest == true}))
            setListCategory(category)
            // console.log('coba result : ',list)
        }
        setRefreshing(false)
    }, [props.getProductStore.payload.data])

    const getData = async() => {
        try {
          const value = await AsyncStorage.getItem('@token')
          const name = await AsyncStorage.getItem('@username')
          if(value !== null) {
            console.log('token : ' + value)
            console.log('nama : ' + name)
            setUsername(name)
            setToken(value)
            handleGetProduct(value,name)
          }
        } catch(e) {
            console.error(e)
        }
    }

    const handleGetProduct = async(token, username) => {
        try{
          props.dispatchGetProduct(token, username)
          console.log('handleAllTask : ', token)
        //   console.log(props.getProductStore.payload.data.foods)
        } catch(e) {
          alert('Your Session Expired, Please Login')
          navigation.navigate('welcomeScreen')
        }
    }

    const Item = ({item, onPress, color, div, style, ...others}) => (
        <TouchableOpacity
          onPress={onPress}
          style={[
            {height: 42, backgroundColor : '#FFFFFF'},
            style,
          ]}>
          <View
            style={{
              flex: 1,
              alignContent: 'center',
            }}>
            <Text
              style={[
                {
                  lineHeight: 32,
                  fontWeight: '500',
                  letterSpacing: -0.02,
                },
                style,
              ]}
              {...others}>
              {item.name}
            </Text>
            {/* <Divider style={{ backgroundColor: '#FFA451', height : {div}, width : 30}} /> */}
            
          </View>
        </TouchableOpacity>
      );

      const handleGetCategory = (id) => {
        switch(id){
            case 1 : {
                return (
                    setListPerCategory(list.filter(obj => {return obj.hottest == true}))
                )
            }
            case 2 : {
                return (
                    setListPerCategory(list.filter(obj => {return obj.popular == true}))
                )
            }
            case 3 : {
                return (
                    setListPerCategory(list.filter(obj => {return obj.newCombo == true}))
                )
            }
            case 4 : {
                return (
                    setListPerCategory(list.filter(obj => {return obj.top == true}))
                )
            }
            default : {
                return (
                    setListPerCategory([])
                )
            }
        }
      }

      const renderButton = ({item}) => {
        const width = item.id === selectedId ? 160 : 100;
        const marginLeft = item.id === selectedId ? 10 : 0;
        const color = item.id === selectedId ? '#27214D' : '#938DB5';
        const fontSize = item.id === selectedId ? 24 : 16;
        // const div = item.id === selectedId ? 2 : 0; 
        // const check = item.ud === selectedId ? true : false;
        return (
          <Item
            item={item}
            onPress={() => {
              setSelectedId(item.id);
              handleGetCategory(item.id)
              toTop()
            }}
            style={{width, marginLeft, color, fontSize}}
          />
        );
      };

    const handleFavorite = (id) => {{
        props.addToFavorite(id)
    }}

    const removeItemValue = async() => {
        const {navigation} = props
        try {
            const value = await AsyncStorage.setItem('@token','');
            await AsyncStorage.setItem('@username','')
            await AsyncStorage.clear()
            props.authStore.payload = {}
            props.getProductStore.payload = {}
            props.basketStore.addedItems = []
            console.log('token : ' + value)
            navigation.navigate('welcomeScreen')
            // return true;
            alert(`You're Logged Out`)
        }
        catch(exception) {
            return false;
        }
      }


    const renderItem = ({ item }) => {
        let existed_fav = props.getProductStore.favorite.find(tes => item.id === tes.id)
        return(
          <TouchableOpacity style={{flex : 1, height:270, width : '100%', marginLeft : -10}} onPress={() => {
            console.log('yang dibuka dari onPress: ', item.id)
            // handleClick(item.id, item.completed)
            // alert(item.id)
            moveScreen({id : item.id})
            }}>
            <Card 
                containerStyle={{
                    backgroundColor :'#FFFFFF', 
                    borderRadius: 16, 
                    borderColor : '#FFFFFF', 
                    height:240, 
                    width:200,
                    shadowColor: "#202020",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.5,
                    shadowRadius: 3.84,
                    elevation: 5,}}>
              <Icons name={existed_fav ? 'heart' : 'heart-outline'} color={'#FFA451'} size={25} style={{position:'absolute', alignSelf:'flex-end'}}
                onPress={() => handleFavorite(item.id)}/>          
              <View style={{alignItems: 'center',  justifyContent:'center', marginTop:8}}>
                  <Image 
                    source={{uri : item.img}}
                    style={{height: 90, width: 90, borderRadius: 90/2, marginTop:6}}
                  />
              </View>
              <View style={{height:70}}>
                <Text style={{marginBottom: 10, 
                        marginTop : 10, 
                        color: '#27214D', 
                        fontSize : 16, 
                        fontWeight : 'bold', 
                        letterSpacing:-0.01,
                        textAlign : 'center',
                        lineHeight:23}}>
                    {item.name}
                </Text>
              </View>
              <View style={{flexDirection:'row', alignItems: 'center',  justifyContent: 'space-between'}}>
                <Text style={{fontSize:18, color:'#F08626'}}>
                    {item.price}
                </Text>
                <View style={{flexDirection : 'row', width : 75, justifyContent : 'space-between', alignItems : 'center'}}>
                    <Icons name='remove-circle-outline' color={'#333333'} size={25} 
                        onPress = {() => {
                        if(item.quantity === 0){
                            console.log('already minimum amount')
                        } else {
                            // item.quantity = item.quantity - 1
                            console.log(item)
                            // setCount(count - 1)
                            handleSubItem(item.id)
                        }
                    }}/>
                    <Text style={{fontSize : 16, color : '#27214D'}}>
                        {item.quantity}
                    </Text>
                    <Icons name='add-circle' color={'#F08626'} size={25} 
                        onPress = {() => {
                        // item.quantity = item.quantity + 1
                        console.log(item)
                        // setCount(count + 1)
                        handleAddItem(item.id)}}/>  
                </View>
              </View>
            </Card>
          </TouchableOpacity>
        )
    }

    const moveScreen = ({id}) => {
        // const { navigation } = props
        navigation.navigate('AddToBasket', {id : props.getProductStore.payload.data.foods.filter(obj => {return obj.id == id})})
        // alert('harusnya kesini : ', id.id)
        // console.log({id : props.getProductStore.payload.data.foods.filter(obj => {return obj.id == id})})
    }

    const handleAddItem = (id) =>{
        props.addToCart(id); 
    }

    const handleSubItem = (id) =>{
        props.subtractQuantity(id); 
    }

    const renderItemBelow = ({ item }) => {
        let existed_fav = props.getProductStore.favorite.find(tes => item.id === tes.id)
        return(
          <TouchableOpacity style={{flex : 1, height:270, width : '100%', marginLeft : -10}} onPress={() => {
            console.log('yang dibuka dari onPress: ', item.id)
            // handleClick(item.id, item.completed)
            // alert(item.img)
            // navigation.navigate('AddToBasket', {
            //     itemId: item.id})
            moveScreen({id : item.id})
            }}>
            <Card 
                containerStyle={{
                    backgroundColor :'#FFFFFF', 
                    borderRadius: 16, 
                    borderColor : '#FFFFFF', 
                    height:240, 
                    width:200,
                    shadowColor: "#202020",
                    shadowOffset: {
                        width: 0,
                        height: 2,
                    },
                    shadowOpacity: 0.5,
                    shadowRadius: 3.84,
                    elevation: 5,}}>
              <Icons name={existed_fav ? 'heart' : 'heart-outline'} color={'#FFA451'} size={25} style={{position:'absolute', alignSelf:'flex-end'}}
                onPress={() => handleFavorite(item.id)}/>          
              <View style={{alignItems: 'center',  justifyContent:'center', marginTop:8}}>
                  <Image 
                    source={{uri : item.img}}
                    style={{height: 90, width: 90, borderRadius: 90/2, marginTop:6}}
                  />
              </View>
              <View style={{height:70}}>
                <Text style={{marginBottom: 10, 
                        marginTop : 10, 
                        color: '#27214D', 
                        fontSize : 16, 
                        fontWeight : 'bold', 
                        letterSpacing:-0.01,
                        textAlign : 'center',
                        lineHeight:23}}>
                    {item.name}
                </Text>
              </View>
              <View style={{flexDirection:'row', alignItems: 'center',  justifyContent: 'space-between'}}>
                <Text style={{fontSize:18, color:'#F08626'}}>
                    {item.price}
                </Text>
                <View style={{flexDirection : 'row', width : 75, justifyContent : 'space-between', alignItems : 'center'}}>
                    <Icons name='remove-circle-outline' color={'#333333'} size={25} onPress = {() => {
                       if(item.quantity >= 0){
                            // item.count = item.count - 1
                            console.log(item)
                            setCount(count - 1)
                            handleSubItem(item.id)
                        }
                    }}/>
                    <Text style={{fontSize : 16, color : '#27214D'}}>
                        {item.quantity==0 ? 0 : item.quantity}
                    </Text>
                    <Icons name='add-circle' color={'#F08626'} size={25} onPress = {() => {
                        // item.count = item.count + 1
                        console.log(item)
                        setCount(count + 1)
                        handleAddItem(item.id)}}/>  
                </View>
              </View>
            </Card>
          </TouchableOpacity>
        )
    }

    
    // console.log('ini lissssttt', list)
    return(
        <ScrollView style={{flex:1, backgroundColor : '#FFFFFF', padding: 24}}>
            <View style={{flexDirection:'row', marginTop:10, justifyContent:'space-between', alignItems:'center'}}>
                <View>
                    <Icons name='log-out-outline' color='#070648' size={30} style={{alignSelf:'center'}}
                         onPress={() => {
                            // logOut()
                            setModalLogoutVisible(true)
                          }}  
                    />
                    <Text style={{fontSize:10, color:'#27214D', textAlign:'center'}}>Log Out</Text>
                </View>
                <View>
                    <Icons name='basket' color='#FFA451' size={30} style={{alignSelf:'center'}} onPress={() => navigation.navigate('OrderList')}/>
                    <Text style={{fontSize:10, color:'#27214D', textAlign:'center'}}>My Basket</Text>
                </View>
            </View>
            <View>
                <View style={{flexDirection:'row',  alignItems:'center', marginTop:24}}>
                    <MainText text={`Hello ${username}, `} 
                        customStyle={{textAlign:'left', marginTop:0, fontWeight:'400'}}/>
                    <MainText text='What fruit salad' 
                        customStyle={{textAlign:'left', marginTop:0, fontWeight:'bold'}}/>    
                </View>
                <MainText text='combo do you want today?' 
                        customStyle={{textAlign:'left', marginTop:0, fontWeight:'bold'}}/>   
            </View>
            <View style={{flexDirection:'row', marginTop:24}}>
                <TextInputwithIcon icon='search-outline' text={'Search for fruit salad combos'} customStyle={{width : '80%'}}/>
                <Icons name='options-outline' color={'#070648'} size={40} style={{alignSelf:'center', paddingLeft:17, width : '20%'}}/>
            </View>
            <MainText text='Recommended Combo' 
                customStyle={{textAlign:'left', 
                    fontWeight:'500',
                    fontSize:24,
                    lineHeight:32,
                    marginTop:40}}/>
            <View style={{marginTop:10}}>
                <FlatList
                    data = {listRecommended}
                    showsHorizontalScrollIndicator={false}
                    horizontal = {true}
                    renderItem={renderItem}
                    keyExtractor={list => list.name}
                    refreshing={refreshing}
                    // onRefresh={getData}
                />    
            </View>
            <View style={{marginTop:10}}>
                <FlatList
                    data = {listCategory} 
                    showsHorizontalScrollIndicator={false}
                    horizontal = {true}
                    renderItem={renderButton}
                    keyExtractor={(item) => item.id.toString()}
                    refreshing={refreshing}
                    extraData={selectedId}
                    // onRefresh={getData}
                />    
            </View> 
            <View style={{marginTop:10, marginBottom : 50}}>
                <FlatList
                    ref={flatListRef}
                    data = {listPerCategory}
                    showsHorizontalScrollIndicator={false}
                    horizontal = {true}
                    renderItem={renderItemBelow}
                    keyExtractor={(item) => item.id.toString()}
                    refreshing={refreshing}
                    // extraData={selectedId}
                    // onRefresh={getData}
                />    
            </View>
            <ModalDelete 
                visible={modalLogoutVisible}
                title='Action Confirmation'
                buttonText='Log Out'
                buttonColor='#FFA451' 
                textColor='#FFFFFF' 
                onRequestClose={() => setModalLogoutVisible(!modalLogoutVisible)} 
                onPress={() => {
                removeItemValue()
                setModalLogoutVisible(!modalLogoutVisible);
                }}
            />           
        </ScrollView>
    )
}

function mapStateToProps(state) {
    return {
        getProductStore: state.getProductStore,
        basketStore : state.basketStore,
        isLoading: state.getProductStore.isLoading,
        authStore : state.authStore,
    }
}

  
function mapDispatchToProps(dispatch) {
    return {
        dispatchGetProduct: (token, username) => dispatch(fetchGetProduct(token, username)),
        addToCart: (id)=>{dispatch(addToCart(id))},
        subtractQuantity : (id)=>{dispatch(subtractQuantity(id))},
        addToFavorite : (id) =>{dispatch(addtoFavorite(id))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (HomeScreen)