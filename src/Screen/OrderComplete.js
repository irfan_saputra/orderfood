import { NavigationHelpersContext, useLinkProps } from '@react-navigation/native';
import React from 'react';
import {Text, View, StyleSheet, Image, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'
import Button from '../component/button'

import CircleGreen0 from '../assets/Rectangle40.png'
import CircleGreen1 from '../assets/Rectangle41.png'

const OrderComplete = (props) => {
    const { navigation } = props
    return(
    <View style={{backgroundColor : '#FFFFFF', flex : 1}}>
        <View style={style.container}>
            <View style={{height : 164, width : 164, borderRadius : 164/2, backgroundColor : '#E0FFE5', borderColor : '#4CD964', borderWidth : 1, alignItems : 'center', justifyContent : 'center'}}>
                <View style={{height : 100, width : 100, borderRadius : 100/2, backgroundColor : '#4CD964', borderColor : '#4CD964', borderWidth : 1, alignItems : 'center', justifyContent : 'center'}}>
                    <Icon name="checkmark-outline" size={60} style={style.iconcheck}/>
                </View>
            </View>
            <Text style={style.textbold}>Congratulations!!!</Text>
            <Text style={style.textnotif}>Your order have been taken and is being attended to</Text>

            <Button buttonText={`Track Order`} 
                customStyle={{width : 135}}
                onPress = {() => navigation.navigate('TrackOrder')}/>
            <TouchableOpacity style={{width : 180, 
                    backgroundColor : '#FFFFFF', 
                    borderRadius : 10, 
                    height : 56, alignItems : 'center',
                    justifyContent : 'center',
                    marginTop : 50,
                    borderWidth : 1,
                    borderColor : '#FFA451'}}
                    onPress={() => navigation.navigate('Home')}>
                <Text style={{fontSize : 16, color : '#FFA451'}}>
                    Continue Shopping
                </Text>
            </TouchableOpacity>
        </View>
    </View>
    )
}

const style = StyleSheet.create({
    container:{
        alignItems:"center",
        marginTop:120,
        backgroundColor :'#FFFFFF',
        flex : 1
    },
    circlegreen1:{
        position:'absolute',
        width:400,
        height:400,
    },
    circlegreen0:{
        position:'absolute',
        width:360,
        height:360,
        marginVertical:15
    },
    trackbutton:{
        marginTop:100
    },
    continueshopbutton:{
        marginTop:50
    },
    textbold:{
        fontWeight:'bold',
        color : '#27214D',
        fontSize:32,
        marginTop:56
    },
    textnotif:{
        fontSize:20,
        paddingLeft:20,
        paddingRight:20,
        marginTop:10,
        textAlign:"center"
    },
    iconcheck:{
        color:'white',
        position:'absolute',
        marginVertical:32
    }
})

export default OrderComplete;