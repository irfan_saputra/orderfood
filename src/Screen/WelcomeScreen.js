import React, { Component, useState, useEffect } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ImageBackground, TextInput, ScrollView} from 'react-native'
import Logo from '../assets/logosplashscreen.png'
import styles from '../styling/styling'
import first from '../assets/firstpage.png'
import MainText from '../component/MainText'
import SecondaryText from '../component/SecondaryText'
import Button from '../component/button'

function welcomeScreen(props){
    const { navigation } = props
    useEffect(() => {
        // getData()
    }, [])

    // const getData = async() => {
    //     try {
    //       const value = await AsyncStorage.getItem('@token')
    //       if(value !== null) {
    //         console.log('token : ' + value)
    //         navigation.navigate(value ? 'Home' : 'SignIn')
    //       } 
    //     } catch(e) {
    //         console.error(e)
    //     }
    // }

    return(
        <ScrollView style={{flex:1, backgroundColor : '#FFFFFF'}}>
            <View style={{backgroundColor:'#FFA451', flex:0.6, justifyContent:'center', alignItems:'center', alignContent:'center'}}>
                <Image source={first} style={{height: 301, width : 301, marginTop : 155, marginBottom:34}}/>
            </View>
            <View style={{flex:0.4, paddingLeft : 25, paddingRight : 25}}>
                <MainText text='Get The Freshest Fruit Salad Combo' customStyle={{textAlign:'left' , marginTop : 56}}/>
                <SecondaryText text='We deliver the best and freshest fruit salad in town. Order for a combo today!!!'
                    customStyle={{textAlign:'left'}}/>
                <Button buttonText={`Let's Continue`} onPress = {() => navigation.navigate('authentication')}/>
            </View>
        </ScrollView>

    )
}

export default welcomeScreen