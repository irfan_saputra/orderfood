import React, { Component, useState, useEffect } from 'react'
import { Alert, StyleSheet, View, Text, TouchableOpacity, Image, ImageBackground, TextInput, ScrollView} from 'react-native'
import Logo from '../assets/logosplashscreen.png'
import Icons from 'react-native-vector-icons/Ionicons'
import styles from '../styling/styling'
import first from '../assets/firstpage.png'
import MainText from '../component/MainText'
import SecondaryText from '../component/SecondaryText'
import Button from '../component/button'
import { Divider } from 'react-native-elements'
import { connect } from 'react-redux';
import { addtoFavorite } from '../stores/actions/getProductActions'
import { addToCart, subtractQuantity, addFromDetail } from '../stores/actions/basketActions'

function AddToBasket({route, navigation, addFromDetail, getProductStore, basketStore, addToFavorite}){
    // const props = super(props)
    // constructor(props) {
    //     super(props);
    //   }
    const { id } = route.params;
    // const { addToCart } = props
    // const { getProductStore} = props
    // const { id } = route.params
    const [selectedItem, setSelectedItem] = useState(id[0])
    const [count, setCount] = useState(0)
    // const [id, setId] = useState(0)

    // useEffect(() => {
    //     setSelectedItem(id)
    // }, [])
    let existed_fav = getProductStore.favorite.find(tes => selectedItem.id === tes.id)

    useEffect(() => {
        if(getProductStore.payload.data && getProductStore.payload.data.foods){
            basketStore.items = getProductStore.payload.data.foods
            console.log('coba : ', getProductStore.payload.data.foods)
            // setList(props.getProductStore.payload.data.foods)
            // setSelectedItem(props.getProductStore.payload.data.foods.filter(obj => {return obj.id == id}))
            // setListPerCategory(props.getProductStore.payload.data.foods.filter(obj => {return obj.hottest == true}))
            // setListCategory(category)
        }
        // setRefreshing(false)
    }, [getProductStore.payload.data])

    // console.log('ini selected itemnya : ', selectedItem)
    console.log('ini dari sebelah : ', selectedItem)

    const handleAddItem = (id, quantity) =>{
        // alert(quantity)
        if (quantity > 0){
            addFromDetail(id, quantity)
            navigation.navigate('Home')
        } else {
            handleNoItem()
        }
        // const { addToCart } = props
        // addToBasket(id);
        // navigation.navigate('Home')
        // addToCart(id)
    }

    const handleNoItem = () => {
        Alert.alert(
            'Add to Basket',
            'No Item Added, back to Home?', [{
                text: 'Cancel',
                //onPress: () = > console.log('Cancel Pressed')},
                style: 'cancel'
            }, {
                text: 'OK',
                onPress : () => navigation.navigate('Home')
           
            }, ], {
                cancelable: false
            }
         )
         return true;
       } 

    const handleFavorite = (id) => {{
        addToFavorite(id)
    }}   

    

    return(
        <ScrollView contentContainerStyle={{flex:1, backgroundColor : '#FFA451'}}>   
            <TouchableOpacity 
                style={{backgroundColor : '#FFFFFF', 
                    width:100, height : 40, 
                    justifyContent : 'center', 
                    borderRadius: 100,
                    marginTop : 32, marginLeft : 24}} onPress={() => navigation.navigate('Home')}>
                <View style={{flexDirection : 'row'}}>
                    <Icons name='chevron-back-outline' size={25} color={'#000000'}/>
                    <Text style={{fontSize : 16, fontWeight : '500'}}>
                        Go Back
                    </Text>
                </View>
            </TouchableOpacity>
            <View style={{justifyContent:'center', alignItems:'center', alignContent:'center'}}>
                <Image source={{uri : selectedItem.img}} style={{height: 176, width : 176, borderRadius : 176/2, marginTop : 16, marginBottom:32}}/>
            </View>
            <View style={{flex:1}}>
                <View style={{ flex : 1, backgroundColor : '#FFFFFF', borderTopLeftRadius : 30, borderTopRightRadius : 30}}>
                    <MainText text={selectedItem.name} customStyle={{textAlign:'left' , marginTop : 56, fontSize : 32, lineHeight : 32, paddingLeft : 25, paddingRight : 25}}/>
                    <View style={{flexDirection : 'row', justifyContent : 'space-between', alignItems : 'center', marginTop : 32, paddingLeft : 25, paddingRight : 25}}>
                        <View style={{flexDirection : 'row', width : 75, justifyContent : 'space-between', alignItems : 'center'}}>
                            <Icons name='remove-circle-outline' color={'#333333'} size={40}
                                onPress = {() => {
                                    if(selectedItem.quantity > 0){
                                        selectedItem.quantity = selectedItem.quantity - 1
                                        console.log(selectedItem)
                                        setCount(count - 1)
                                        // handleSubItem(selectedItem.id)
                                    }}}
                                />
                            <Text style={{fontSize : 24, color : '#27214D', marginLeft : 16}}>
                                {selectedItem.quantity}
                            </Text>
                            <Icons name='add-circle' color={'#F08626'} size={40} style={{marginLeft : 16}}
                                onPress = {() => {
                                    selectedItem.quantity = selectedItem.quantity + 1
                                    console.log(selectedItem)
                                    setCount(count + 1)
                                    // handleAddItem(selectedItem.id)
                                    }}
                                />  
                        </View>
                            <Text style={{fontSize : 24, color : '#27214D'}}>
                                {selectedItem.price}
                            </Text>
                    </View>
                    
                    <Divider style={{ backgroundColor: '#F3F3F3', marginTop : 32, height : 2}} />
                    <ScrollView>
                        <View style={{paddingLeft : 25, paddingRight : 25}}>
                            <View 
                                style={{ justifyContent : 'space-between', 
                                    marginTop : 32, 
                                    height : 32,
                                    width : 200}}>
                                <MainText text='One Pack Contains :' customStyle={{textAlign:'left', fontSize : 20, marginTop : 0}}/>
                                <Divider style={{ backgroundColor: '#FFA451', height : 2}} />
                            </View>
                            <MainText text={selectedItem.ingredients} customStyle={{textAlign:'left' , fontSize : 16, marginTop : 18}}/>
                        </View>
                        <Divider style={{ backgroundColor: '#F3F3F3', marginTop : 32, height : 2}} />
                        
                        <View style={{paddingLeft : 25, paddingRight : 25, marginBottom : 16}}>
                            <MainText text={selectedItem.description} customStyle={{textAlign:'left' , fontSize : 14, marginTop : 24}}/>
                            
                            <View style={{flexDirection : 'row', justifyContent : 'space-between', marginTop : 40, width : '100%', backgroundColor : 're'}}>
                                <View style={{height : 50, width : 50, borderRadius : 50/2, backgroundColor : '#FFF7F0', alignItems : 'center', justifyContent : 'center'}}>
                                    <Icons name={existed_fav ? 'heart' : 'heart-outline'} color={'#FFA451'} size={30} 
                                        onPress={()=>handleFavorite(selectedItem.id)}
                                    />
                                </View>
                                <Button buttonText={`Add to Basket`} 
                                    customStyle={{marginTop : 0, marginLeft : 24, width : '80%'}}
                                    onPress={() => {handleAddItem(selectedItem.id, selectedItem.quantity) }}/>
                            </View>
                        </View>
                    </ScrollView>
                </View>
            </View>
        </ScrollView>
    )
}

function mapStateToProps(state) {
    return {
        getProductStore: state.getProductStore,
        isLoading: state.getProductStore.isLoading,
        basketStore : state.basketStore
    }
}

  
function mapDispatchToProps(dispatch) {
    return {
        // dispatchGetProduct: (token, username) => dispatch(fetchGetProduct(token, username)),
        // addToBasket: (id)=>{dispatch(addToCart(id))}
        addFromDetail : (id, quantity)=>{dispatch(addFromDetail(id, quantity))},
        addToFavorite : (id) =>{dispatch(addtoFavorite(id))}
    }
}

export default connect(mapStateToProps, mapDispatchToProps) (AddToBasket)