import React from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Icons from 'react-native-vector-icons/Ionicons'
import order from '../assets/ordertaken.png'
import line from '../assets/line.png'
import lanjut from '../assets/lanjut.png'
import deliv from '../assets/deliv.png'
import maps from '../assets/maps.png'


const TrackOrder = (props) =>{
    const { navigation } = props
    return (
        <View style={{flex : 1, backgroundColor : '#FFFFFF'}}> 
            <View style={{backgroundColor : '#FFA451', height : 120, justifyContent: 'space-between' , flexDirection : 'row', alignItems : 'center'}}>
                <TouchableOpacity 
                    style={{backgroundColor : '#FFFFFF', 
                        width:100, height : 40, 
                        justifyContent : 'center', 
                        borderRadius: 100,
                        marginLeft : 24}} 
                        onPress={() => navigation.navigate('Home')}>
                    <View style={{flexDirection : 'row'}}>
                        <Icons name='chevron-back-outline' size={25} color={'#000000'}/>
                        <Text style={{fontSize : 16, fontWeight : '500'}}>
                            Go Back
                        </Text>
                    </View>
                </TouchableOpacity>
                <View style={{marginLeft : 40}}>
                    <Text style={{fontSize : 24, color : '#FFFFFF', fontWeight : '500', textAlign : 'center'}}>
                        Delivery Status
                    </Text>
                </View>
                <View
                    style={{backgroundColor : '#FFA451', 
                        width:100, height : 40, 
                        justifyContent : 'center', 
                        borderRadius: 100,
                        marginRight : 24
                        }}>
                </View>
            </View>

            <View style={{padding : 24}}>
                <View style={{flexDirection : 'row'}}>
                    <View style={{height : 80, width : 80, borderRadius : 10, backgroundColor : '#FFFAEB', alignItems : 'center', justifyContent : 'center'}}>
                        <Image source={order} style={{height: 60, width : 60}}/>
                    </View>
                    <View style={{marginLeft : 10, alignItems : 'center', flexDirection : 'row', width : '80%', justifyContent : 'space-between', paddingRight : 16}}>
                        <Text style={{fontSize : 16, fontWeight : 'bold'}}>
                            Order Taken
                        </Text>
                        <Icons name='checkmark-circle' size={25} color={'#4CD964'}/>
                    </View>
                </View>
                <Image source={line} style={{height: 40, width : 1, marginLeft : 40}}/>
                <View style={{flexDirection : 'row'}}>
                    <View style={{height : 80, width : 80, borderRadius : 10, backgroundColor : '#F1EFF6', alignItems : 'center', justifyContent : 'center'}}>
                        <Image source={lanjut} style={{height: 70, width : 70}}/>
                    </View>
                    <View style={{marginLeft : 10, alignItems : 'center', flexDirection : 'row', width : '80%', justifyContent : 'space-between', paddingRight : 16}}>
                        <Text style={{fontSize : 16, fontWeight : 'bold'}}>
                            Order Is Being Prepared
                        </Text>
                        <Icons name='checkmark-circle' size={25} color={'#4CD964'}/>
                    </View>
                </View>
                <Image source={line} style={{height: 40, width : 1, marginLeft : 40}}/>
                <View style={{flexDirection : 'row'}}>
                    <View style={{height : 80, width : 80, borderRadius : 10, backgroundColor : '#FEF0F0', alignItems : 'center', justifyContent : 'center'}}>
                        <Image source={deliv} style={{height: 70, width : 70}}/>
                    </View>
                    <View style={{marginLeft : 10, alignItems : 'center', flexDirection : 'row', width : '80%', justifyContent : 'space-between', paddingRight : 16}}>
                        <View>
                            <Text style={{fontSize : 16, fontWeight : 'bold'}}>
                                Order Is Being Delivered
                            </Text>
                            <Text style={{fontSize : 12, fontWeight : '500', marginTop : 4}}>
                                Your delivery agent is coming
                            </Text>
                        </View>
                        <View style={{height : 40,
                            width : 40,
                            borderRadius : 40/2,
                            backgroundColor : '#FFA451',
                            alignItems : 'center',
                            justifyContent : 'center'}}>
                            <Icons name='call' size={25} color={'#FFFFFF'}/>
                        </View>
                    </View>
                </View>
                <Image source={line} style={{height: 40, width : 1, marginLeft : 40}}/>
                <Image source={maps} style={{width : '100%', borderRadius : 10}} />
                <Image source={line} style={{height: 40, width : 1, marginLeft : 40}}/>
                <View style={{flexDirection : 'row'}}>
                    <View style={{height : 80, width : 80, borderRadius : 10, backgroundColor : '#F0FEF8', alignItems : 'center', justifyContent : 'center'}}>
                        <Icons name='checkmark-circle' size={50} color={'#4CD964'}/>
                    </View>
                    <View style={{marginLeft : 10, alignItems : 'center', flexDirection : 'row', width : '80%', justifyContent : 'space-between', paddingRight : 16}}>
                        <Text style={{fontSize : 16, fontWeight : 'bold'}}>
                            Order Received
                        </Text>
                        {/* <Icons name='checkmark-circle' size={25} color={'#4CD964'}/> */}
                        <View style={{flexDirection : 'row'}}>
                            <View style={{height : 8, width : 8, borderRadius : 8/2, backgroundColor : '#FFE3C9'}}></View>
                            <View style={{height : 8, width : 8, borderRadius : 8/2, backgroundColor : '#FFE3C9', marginLeft : 8}}></View>
                            <View style={{height : 8, width : 8, borderRadius : 8/2, backgroundColor : '#FFE3C9', marginLeft : 8}}></View>
                        </View>
                    </View>
                </View>
            </View>

        </View>      
    )
}

const style = StyleSheet.create({
    container:{
        flex:1,
        alignItems:'center'
    },
    headerOrange:{
        backgroundColor:'#FFA451',
        height:100,
        alignSelf:'stretch',
    },
    textTitle:{
        position:'absolute',
        color:'white',
        fontSize:14,
        marginTop:50
    },
    button:{
        position:'absolute',
        marginTop:20
    },
}) 

export default TrackOrder;