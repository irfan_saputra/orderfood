import { combineReducers } from 'redux'

import authStoreReducer from './authStoreReducers'
import getProductReducer from './getProductReducers'
import basketReducer from './basketReducers'

const reducers = {
    authStore : authStoreReducer,
    getProductStore : getProductReducer,
    basketStore : basketReducer
}

const rootReducer = combineReducers(reducers)

export default rootReducer