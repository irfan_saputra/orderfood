import { ADD_TO_CART } from '../actions/basketActions'

const defaultState = {
    items : [],
    addedItems : [],
    total: 0,
    isLoading : false,
    error : {}
}

export default (state = defaultState, action = {}) => {
    switch (action.type){
        case 'ADD_TO_CART': {
          let addedItem = state.items.find(item=> item.id === action.id)
                //check if the action id exists in the addedItems
          let existed_item= state.addedItems.find(item=> action.id === item.id)
          if(existed_item){
            addedItem.quantity += 1 
            return{
              ...state,
              total: state.total + addedItem.price 
            }
          } else {
              addedItem.quantity = 1;
              //calculating the total
              let newTotal = state.total + addedItem.price 
              return{
                ...state,
                addedItems: [...state.addedItems, addedItem],
                total : newTotal
              }      
          }
        }

        case 'ADD_FROM_DETAIL': {
          let addedItem = state.items.find(item=> item.id === action.id)
                //check if the action id exists in the addedItems
          let existed_item= state.addedItems.find(item=> action.id === item.id)
          if(existed_item){
            // addedItem.quantity += 1 
            addedItem.quantity = addedItem.quantity + action.quantity
            return{
              ...state,
              total: state.total + addedItem.price*action.quantity
            }
          } else {
              addedItem.quantity = action.quantity;
              //calculating the total
              let newTotal = state.total + addedItem.price*action.quantity
              return{
                ...state,
                addedItems: [...state.addedItems, addedItem],
                total : newTotal
              }                      
          }
        }

        case 'SUB_QUANTITY': {
          let existed_item = state.addedItems.find(item => action.id === item.id)
          if (existed_item){
            let addedItem = state.items.find(item=> item.id === action.id) 
            //if the qt == 0 then it should be removed
            if(addedItem.quantity === 1){
                let new_items = state.addedItems.filter(item=>item.id !== action.id)
                // if (state.total > 0){
                  addedItem.quantity = 0
                  let newTotal = state.total - addedItem.price
                    return{
                        ...state,
                        addedItems: new_items,
                        total: newTotal
                    } 
                  } else {
                      addedItem.quantity -= 1
                      let newTotal = state.total - addedItem.price
                      return{
                        ...state,
                        total : newTotal
                      }
                  }
              } else {
                return{
                  ...state
                }
              }
            }
          
          default:
            return state;

    }
    // INSIDE HOME COMPONENT
//     if(action.type === ADD_TO_CART){
//       let addedItem = state.items.find(item=> item.id === action.id)
//       //check if the action id exists in the addedItems
//      let existed_item= state.addedItems.find(item=> action.id === item.id)
//      if(existed_item)
//      {
//         addedItem.quantity += 1 
//          return{
//             ...state,
//              total: state.total + addedItem.price 
//               }
//     }
//      else{
//         addedItem.quantity = 1;
//         //calculating the total
//         let newTotal = state.total + addedItem.price 
        
//         return{
//             ...state,
//             addedItems: [...state.addedItems, addedItem],
//             total : newTotal
//         }
        
//     }
// }
// else{
//     return state
// }
}