const defaultState = {
    payload : {},
    favorite : [],
    isLoading : false,
    error : {},
    total : 0
}

export default (state = defaultState, action = {}) => {
    switch (action.type){
        case 'FETCH_GETALLTASK_REQUEST': {
            return {
              ...state,
              isLoading: true
            };
          }
      
          case 'FETCH_GETALLTASK_SUCCESS': {
            return {
              ...state,
              payload: action.payload,
              isLoading: false
            };
          }
      
          case 'FETCH_GETALLTASK_FAILED': {
            return {
              ...state,
              payload: {},
              error: action.error,
              isLoading: false
            };
          }

          case 'ADD_TO_FAVORITE' : {
            let favItem = state.payload.data.foods.find(item=> item.id === action.id)
            let new_items = state.favorite.filter(item => item.id !== action.id)
            if (favItem.favorite == false){
              favItem.favorite = true
              return {
                ...state,
                favorite : [...state.favorite, favItem]
              }
            } else {
              favItem.favorite = false
              return{
                ...state,
                favorite : new_items
              }
            } 
          }

          
        //   case 'ADD_TO_CART' : {
        //     let basket = state.payload.data.foods.find(item=> item.id === action.id)
        //     //check if the action id exists in the addedItems
        //     let existed_item= state.basket.find(item=> action.id === item.id)
        //     if(existed_item){
        //       basket.quantity += 1 
        //       return{
        //         ...state,
        //         total: state.total + basket.price 
        //       }
        // }
          // }


          default:
            return state;
    }
}