import getProduct from '../../api/getProduct'

function fetchGetProduct(token, name){
    return async (dispatch) => {
        dispatch({
            type: 'FETCH_GETALLTASK_REQUEST'
        })

        try {
            const result = await getProduct(token, name)

            if (result.status === 200){
                // let hasil = result.data.map(function(o) {
                // o.count = 0
                // return o
                // })
                dispatch({
                    type : 'FETCH_GETALLTASK_SUCCESS',
                    payload : result.data
                })
            } else {
                dispatch({
                    type : 'FETCH_GETALLTASK_FAILED',
                    payload : result.data
                })
            }
        } catch (error){
            dispatch({
                type : 'FETCH_GETALLTASK_FAILED',
                error : error
            })
        }
    }
}

export function addtoFavorite(id){
    return async (dispatch) => {
        dispatch({
            type : 'ADD_TO_FAVORITE',
            id,
        })
    }
}

export default fetchGetProduct