export function addToCart(id){
    return async (dispatch) => {
        dispatch({
            type : 'ADD_TO_CART',
            id
        })
    }
}

export function subtractQuantity(id){
    return async (dispatch) => {
        dispatch({
            type : 'SUB_QUANTITY',
            id
        })
    }
}

export function addFromDetail(id, quantity){
    return async (dispatch) => {
        dispatch({
            type : 'ADD_FROM_DETAIL',
            id,
            quantity
        })
    }
}