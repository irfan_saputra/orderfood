import React, { useState, useEffect } from 'react'
import { TouchableOpacity, Text, View, TextInput } from 'react-native'
import Icons from 'react-native-vector-icons/Ionicons';
import styles from '../styling/styling'

function TextInputwithIcon(props){
    const { text, icon, ...others} = props


    return(
        <View style={[styles.textInput, props.customStyle]} {...others}>
            <Icons style={{}} name={icon} size={20} color='#86869E'/>
            <TextInput 
                placeholder={text}
                placeholderTextColor='#86869E'
                style={{
                    height : 56,
                    width : 230,
                    fontSize:14,
                    lineHeight : 21,
                    paddingLeft : 19}} {...others} 
                    />     
        </View>
    )

}

export default TextInputwithIcon