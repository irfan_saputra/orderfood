import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import styles from '../styling/styling'

function SecondaryText(props){
    const { text, ...others} = props

    return(
        <Text style={[
            styles.secondaryText,
            props.customStyle
            ]}
            {...others}>
            {text}
        </Text>
    )
}

export default SecondaryText