import React, { useState, useEffect} from 'react'
import { Modal, TouchableOpacity, Text, View, TextInput } from 'react-native'
import styles from '../styling/styling'
import Icons from 'react-native-vector-icons/Ionicons';

function ModalDelete(props){
    const { buttonText, title, textColor, placeholderText, buttonColor, icon, invert, disable, onDone, onDelete, setModal, ...others} = props
    const [modalVisible, setModalVisible] = useState(false)

    return(
          <View> 
            <Modal
              animationType="slide"
              transparent={true}
              {...others}
            >
              <View style={styles.centeredView}>
                <View style={styles.modalView}>
                  <Text style={styles.modalText}>{title}</Text>
                  <TouchableOpacity
                      style={{ ...styles.openButton, backgroundColor: buttonColor, borderColor : '#FFA451', borderWidth: 1, marginTop : 12 }}
                      {...others}
                    >
                      <Text style={{...styles.textStyle, color:textColor}}>{buttonText}</Text>
                    </TouchableOpacity>
                </View>
              </View>
            </Modal>
          </View>
    )
}

export default ModalDelete