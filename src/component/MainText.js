import React from 'react'
import { TouchableOpacity, Text } from 'react-native'
import styles from '../styling/styling'

function MainText(props){
    const { text, ...others} = props

    return(
        <Text style={[
            styles.mainText,
            props.customStyle
            ]}
            {...others}>
            {text}
        </Text>
    )
}

export default MainText