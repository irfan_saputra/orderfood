import React from 'react'
import { TouchableOpacity, Text, View } from 'react-native'
import styles from '../styling/styling'
import Icons from 'react-native-vector-icons/Ionicons';

function Button(props){
    const { buttonText, icon, invert, disable, padd, ...others} = props

    return(
        <TouchableOpacity style={[
            styles.button,
            invert && styles.invert,
            props.customStyle
            ]}
            {...others}>
            <View style={{
                flexDirection:'row',
                justifyContent : 'center',
                alignItems: 'center'}}>    
                <Icons style={{paddingRight : padd}} name={icon} size={20} color='#FFFFFF'/>       
                <Text style={styles.textButton}{...others}>{buttonText}</Text>
            </View>    
        </TouchableOpacity>
    )
}

export default Button