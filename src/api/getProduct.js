import fetchApi from '../utils/fetchApi'

async function getProduct(token, username){
    try {
        const result = await fetchApi('GET', token, 'home?username=', username, {})
        console.log(result)
        return result
    } catch (error){
        throw error
    }
}

export default getProduct