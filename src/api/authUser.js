import fetchApi from '../utils/fetchApi'


async function login({username}){
    try {
        const result = await fetchApi('POST','', 'login','', {username})
        console.log(result)
        return result
    } catch (error){
        alert('Login Failed, ' +  error)
    }
}

export default login