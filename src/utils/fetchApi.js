import axios from 'axios'
import Config from 'react-native-config'

async function fetchApi(method, auth, path, username, body){
    try{
        const response = await axios({method,
                                    url : `${Config.BASE_URL}/${path}${username}`,
                                    // url : `https://api.irwinpratajaya.com/login`,
                                    headers : {'Authorization' : `${auth}`},
                                    data : body})
        if (response.status != 400){
            return response
        }
        console.log(response)
    } catch (error){
        throw error
    }
}

export default fetchApi