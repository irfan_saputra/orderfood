import * as React from 'react';
import { View, Text } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import OrderComplete from '../screen/OrderComplete';
import TrackOrder from '../screen/TrackOrder';
import splashScreen from '../screen/SplashScreen'
import welcomeScreen from '../screen/WelcomeScreen'
import authentication from '../screen/Authentication'
import HomeScreen from '../screen/HomeScreen'
import AddToBasket from '../screen/AddToBasket'
import OrderList from '../screen/OrderList'



const Stack = createStackNavigator();

function MainNavigation() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="splashScreen" screenOptions={{headerShown: false}}>
        <Stack.Screen name='welcomeScreen' component={welcomeScreen} />
        <Stack.Screen name='authentication' component={authentication} />
        <Stack.Screen name='splashScreen' component={splashScreen} />
        <Stack.Screen name="OrderComplete" component={OrderComplete} />
        <Stack.Screen name="TrackOrder" component={TrackOrder} />
        <Stack.Screen name="AddToBasket" component={AddToBasket} />
        <Stack.Screen name="OrderList" component={OrderList} />
        <Stack.Screen name="Home" component={HomeScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default MainNavigation;