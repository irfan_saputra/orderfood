import { StyleSheet } from 'react-native'

const styles = StyleSheet.create({
    centerImageSplash : {
        flex: 1, backgroundColor:'#FFFFFF', 
        justifyContent:'center', 
        alignItems:'center', 
        alignContent:'center'
    },
    button : {
        backgroundColor : '#FFA451',
        borderRadius : 10,
        marginTop : 58,
        height : 56,
        width : '100%',
        justifyContent : 'center',
        alignSelf : 'center'
    },
    invert : {
        backgroundColor : 'red'
    },
    mainText : {
        // alignSelf : 'center', 
        fontWeight : '500', 
        textAlign : 'center',
        fontSize : 20,
        letterSpacing : -0.01,
        lineHeight : 29,
        marginTop : 48,
        color : '#27214D'
    },
    secondaryText : {
        // alignSelf : 'center', 
        // fontWeight : '500', 
        textAlign : 'center',
        fontSize : 16,
        letterSpacing : -0.01,
        lineHeight : 24,
        marginTop : 16,
        color : '#5D577E'
    },
    textButton : {
        fontSize : 16,
        textAlign: 'center',
        color : '#FFFFFF',
        lineHeight : 24,
        fontWeight : '500',
        letterSpacing : -0.01
    },
    textInput : {
        flexDirection:'row',
        justifyContent : 'center',
        alignItems: 'center',
        backgroundColor : '#F3F1F1',
        borderRadius : 10,
        width : 288
    },
    textInputPassword : {
        flexDirection:'row',
        justifyContent : 'space-evenly',
        alignItems: 'center',
        marginTop : 16,
        borderColor:'#D0DBEA',
        borderRadius : 32,
        borderWidth: 1,
        marginLeft : 24,
        marginRight : 24
    },
    checkBox : {
        flexDirection:'row',
        marginLeft : 24,
        marginTop : 16,
        alignItems: 'center'
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
      },
      modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
          width: 0,
          height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
      },
      openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width : 125
      },
      dualButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2,
        width : 70
      },
      textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
      },
      modalText: {
        marginBottom: 15,
        textAlign: "center",
        color : '#2E3E5C'
      }
})

export default styles