import React, { Component, useState, useEffect } from 'react'
import { StyleSheet, View, Text, TouchableOpacity, Image, ImageBackground, TextInput} from 'react-native'
import Logo from '../assets/logosplashscreen.png'
import styles from '../styling/styling'
import AsyncStorage from '@react-native-async-storage/async-storage';

function splashScreen(props){
    const { navigation } = props
    useEffect(() => {
        setTimeout(()=> {
            getData()
            navigation.navigate("welcomeScreen");
            },3000
        );
    }, [])

    const getData = async() => {
        try {
          const value = await AsyncStorage.getItem('@token')
        //   await AsyncStorage.setItem('@token','');
        //   await AsyncStorage.clear()
        //   const value = await AsyncStorage.getItem('@token')
        //   if(value !== null) {
        //     console.log('token : ' + value)
            //navigation.navigate(value ? 'Home' : 'welcomeScreen')
        //   }
        if(value !== null) {
            console.log('token : ' + value)
            navigation.navigate(value ? 'Home' : 'welcomeScreen')
          }  
        } catch(e) {
            console.error(e)
        }
    }

    // const getData = async() => {
    //     try {
    //       const value = await AsyncStorage.getItem('@token')
    //       if(value !== null) {
    //         console.log('token : ' + value)
    //         navigation.navigate(value ? 'Home' : 'SignIn')
    //       } 
    //     } catch(e) {
    //         console.error(e)
    //     }
    // }

    return(
        <View style={styles.centerImageSplash}>
            <Image source={Logo} style={{height: 206, width : 184}}/>
        </View>
    )
}

export default splashScreen