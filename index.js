/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import splashScreen from './src/screen/SplashScreen'
import AddToBasket from './src/screen/AddToBasket'

AppRegistry.registerComponent(appName, () => App);