# OrderFood-App

Use this Figma URL to Design : https://www.figma.com/file/1q5RTPm1RKIB8bQf8DdMMK/Mobile-UI-Kit%3A-Ecommerce-(Community)?node-id=12%3A0

OrderFood App Task

### Requirement
#### Page
1. [Splash Screen](#splashscreen)
2. [Welcome Screen](#welcome)
3. [Authentication](#auth)
4. [Home Screen](#home)
5. [Add to Basket](#addtobasket)
6. [Order List](#orderlist)
7. [Complete Detail (input address and contact number)](#completedetail)
8. [Order Complete](#ordercomplete)
9. [Track Order](#trackorder)
10. [Input Card Detail](#inputcard)

###### SplashScreen <a name="splashscreen"></a>

###### Welcome <a name="welcome"></a>


###### Authentication <a name="auth"></a>
- input name
- save to redux
- add button to home screen

###### Home <a name="home"></a>
- get product from API
- add search function
- add button to basket
- go to detail product in add to basket

###### Add to Basket <a name="addtobasket"></a>
- Show detail product
- Add to basket

###### Order List <a name="orderlist"></a>
- Show Order list

###### Complete Details <a name="completedetail"></a>
- Add raw bottom sheet react

###### Order Complete <a name="ordercomplete"></a>
- Static order complete page

###### Track Order <a name="trackorder"></a>
- static track order

###### Input Card Details <a name="inputcard"></a>
- static input card for auth user
